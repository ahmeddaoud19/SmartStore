﻿

   --------------------------------------------------------------------------------
              README file for LibSass Host Native for Windows x86 v1.2.0

   --------------------------------------------------------------------------------

           Copyright (c) 2015-2018 Andrey Taritsyn - http://www.taritsyn.ru


   ===========
   DESCRIPTION
   ===========
   This package complements the LibSassHost package and contains the native
   implementation of LibSass version 3.5.5 for Windows (x86).

   For correct working of the LibSass require the Microsoft Visual C++
   Redistributable for Visual Studio 2017.

   =============
   RELEASE NOTES
   =============
   1. Added support of the LibSass version 3.5.5;
   2. Now the LibSass for Windows requires the Microsoft Visual C++ Redistributable
      for Visual Studio 2017.

   ====================
   POST-INSTALL ACTIONS
   ====================
   If in your system does not `api-ms-win-core-*.dll`, `api-ms-win-crt-*.dll`,
   `concrt140.dll`, `msvcp140.dll`, `ucrtbase.dll` and `vcruntime140.dll`
   assemblies, then download and install the Microsoft Visual C++ Redistributable
   for Visual Studio 2017
   (https://www.visualstudio.com/downloads/#microsoft-visual-c-redistributable-for-visual-studio-2017).

   ============
   PROJECT SITE
   ============
   http://github.com/Taritsyn/LibSassHost